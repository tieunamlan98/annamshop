<?php
include_once ("Inc/header.php");
include_once ("Inc/slide.php");
?>
<div class="container">
         <div class="row">
            <div id="content" class="col-sm-12">
               <div class="content_top">
                  <div class="position-display">
                     <div class="show-in-tab-mod">
                        <div class="non-show">
                           <div><i class="fa fa-frown-o"></i></div>
                           Xin lỗi, "Hiển thị danh mục trong tabs!" không thể hiển thị trong cột
                        </div>
                        <div class="show-in-tab">
                           <ul class="nav nav-tabs">
                              <li class="active"><a href="#tab-507b14-0" data-toggle="tab" aria-expanded="true">Khuyến mãi</a></li>
                              <li><a href="#tab-507b14-1" data-toggle="tab" aria-expanded="true">Nổi Bật</a></li>
                              <li><a href="#tab-507b14-3" data-toggle="tab" aria-expanded="true">Bán chạy</a></li>
                              <!-- <li class="empty_tab"><a>&nbsp;</a></li>-->
                           </ul>
                        </div>
                        <div class="tab-content row">
                           <div id="tab-507b14-0" role="tabpanel" class="tab-pane active">
                              <div class="owl-car507b14 nrb-next-prev">
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                         
                                       </div>
                                    </div>
                                 </div>
                              <div class="tab-links col-lg-12">
                              </div>
                           </div>
                           <div id="tab-507b14-1" role="tabpanel" class="tab-pane ">
                              <div class="owl-car507b14 nrb-next-prev">
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/mien-ga">
                                             <img src="Content/img/pho_9-300x300.jpg" alt="Miến gà" title="Miến gà" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/mien-ga">Miến gà</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">25.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">42.500 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('9');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/mien-ga" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('9');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('9');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/bun-bo-hue-to-lon">
                                             <img src="Content/img/pho_8-300x300.jpg" alt="Bún bò huế tô lớn" title="Bún bò huế tô lớn" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/bun-bo-hue-to-lon">Bún bò huế tô lớn</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">26.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">21.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('8');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/bun-bo-hue-to-lon" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('8');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('8');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho-bo-tai-quang-ninh">
                                             <img src="Content/img/pho_13-300x300.jpg" alt="Phở bò tái Quảng Ninh" title="Phở bò tái Quảng Ninh" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho-bo-tai-quang-ninh">Phở bò tái Quảng Ninh</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">30.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">33.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('13');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho-bo-tai-quang-ninh" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('13');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('13');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho-bo-tai-nha-trang">
                                             <img src="Content/img/pho_7-300x300.jpg" alt="Phở bò tái Nha Trang" title="Phở bò tái Nha Trang" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho-bo-tai-nha-trang">Phở bò tái Nha Trang</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">30.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">52.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('7');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho-bo-tai-nha-trang" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('7');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('7');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho-ga">
                                             <img src="Content/img/pho_17-300x300.jpg" alt="Phở gà" title="Phở gà" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho-ga">Phở gà</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">20.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">22.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('17');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho-ga" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('17');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('17');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho-bo-sai-gon">
                                             <img src="Content/img/pho_10-300x300.jpg" alt="Phở bò Sài Gòn" title="Phở bò Sài Gòn" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho-bo-sai-gon">Phở bò Sài Gòn</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">18.500 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('10');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho-bo-sai-gon" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('10');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('10');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/bun-bo-hue">
                                             <img src="Content/img/pho_6-300x300.jpg" alt="Bún bò huế" title="Bún bò huế" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/bun-bo-hue">Bún bò huế</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">20.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">32.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('6');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/bun-bo-hue" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('6');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('6');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho-bo-nam-cha">
                                             <img src="Content/img/pho_12-300x300.jpg" alt="Phở bò nạm chả" title="Phở bò nạm chả" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho-bo-nam-cha">Phở bò nạm chả</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">12.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('12');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho-bo-nam-cha" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('12');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('12');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/bun-bo-mien-trung">
                                             <img src="Content/img/pho_15-300x300.jpg" alt="Bún bò miền Trung" title="Bún bò miền Trung" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/bun-bo-mien-trung">Bún bò miền Trung</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">37.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('15');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/bun-bo-mien-trung" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('15');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('15');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho-kho-hen">
                                             <img src="Content/img/pho_20-300x300.jpg" alt="Phở khô hến" title="Phở khô hến" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho-kho-hen">Phở khô hến</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">65.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('20');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho-kho-hen" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('20');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('20');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho--bo-nam-tay-nguyen">
                                             <img src="Content/img/pho_14-300x300.jpg" alt="Phở bò Nạm Tây Nguyên" title="Phở bò Nạm Tây Nguyên" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho--bo-nam-tay-nguyen">Phở bò Nạm Tây Nguyên</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">24.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('14');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho--bo-nam-tay-nguyen" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('14');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('14');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho-bo-tai">
                                             <img src="Content/img/pho_5-300x300.jpg" alt="Phở bò tái" title="Phở bò tái" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho-bo-tai">Phở bò tái</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">24.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">25.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('5');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho-bo-tai" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('5');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('5');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="tab-links col-lg-12">
                              </div>
                           </div>
                           <div id="tab-507b14-3" role="tabpanel" class="tab-pane ">
                              <div class="owl-car507b14 nrb-next-prev">
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/bun-bo-hue-to-lon">
                                             <img src="Content/img/pho_8-300x300.jpg" alt="Bún bò huế tô lớn" title="Bún bò huế tô lớn" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/bun-bo-hue-to-lon">Bún bò huế tô lớn</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">26.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">21.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('8');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/bun-bo-hue-to-lon" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('8');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('8');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho-bo-sai-gon">
                                             <img src="Content/img/pho_10-300x300.jpg" alt="Phở bò Sài Gòn" title="Phở bò Sài Gòn" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho-bo-sai-gon">Phở bò Sài Gòn</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">18.500 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('10');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho-bo-sai-gon" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('10');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('10');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/bun-bo-hue">
                                             <img src="Content/img/pho_6-300x300.jpg" alt="Bún bò huế" title="Bún bò huế" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/bun-bo-hue">Bún bò huế</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">20.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">32.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('6');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/bun-bo-hue" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('6');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('6');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/mien-ga">
                                             <img src="Content/img/pho_9-300x300.jpg" alt="Miến gà" title="Miến gà" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/mien-ga">Miến gà</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">25.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">42.500 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('9');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/mien-ga" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('9');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('9');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho-ga">
                                             <img src="Content/img/pho_17-300x300.jpg" alt="Phở gà" title="Phở gà" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho-ga">Phở gà</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">20.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">22.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('17');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho-ga" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('17');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('17');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/bun-bo-gio-heo">
                                             <img src="Content/img/pho_4-300x300.jpg" alt="Phở bò chả cá" title="Phở bò chả cá" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/bun-bo-gio-heo">Phở bò chả cá</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">18.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">41.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('4');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/bun-bo-gio-heo" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('4');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('4');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho-bo-tai-nha-trang">
                                             <img src="Content/img/pho_7-300x300.jpg" alt="Phở bò tái Nha Trang" title="Phở bò tái Nha Trang" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho-bo-tai-nha-trang">Phở bò tái Nha Trang</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">30.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">52.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('7');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho-bo-tai-nha-trang" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('7');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('7');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="p-sign color_overlay">
                                             Khuyến mãi
                                          </div>
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho-bo-tai">
                                             <img src="Content/img/pho_5-300x300.jpg" alt="Phở bò tái" title="Phở bò tái" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho-bo-tai">Phở bò tái</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">24.000 VNĐ</span>
                                                <div class="clear"></div>
                                                <span class="price-old">25.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('5');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho-bo-tai" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('5');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('5');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/bun-bo-mien-trung">
                                             <img src="Content/img/pho_15-300x300.jpg" alt="Bún bò miền Trung" title="Bún bò miền Trung" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/bun-bo-mien-trung">Bún bò miền Trung</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">37.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('15');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/bun-bo-mien-trung" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('15');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('15');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                       <div class="t-all-product-info">
                                          <div class="t-product-img">
                                             <a href="http://19671.chilibusiness.net/pho-bo-nam-cha">
                                             <img src="Content/img/pho_12-300x300.jpg" alt="Phở bò nạm chả" title="Phở bò nạm chả" class="img-responsive">
                                             </a>
                                          </div>
                                          <div class="tab-p-info">
                                             <a href="http://19671.chilibusiness.net/pho-bo-nam-cha">Phở bò nạm chả</a>
                                             <div class="star">
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             </div>
                                             <div class="price_product">
                                                <span class="price-new">12.000 VNĐ</span>
                                             </div>
                                             <div class="al-btns">
                                                <button type="button" onclick="cart.add('12');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                                <ul class="add-to-links">
                                                   <li><a href="http://19671.chilibusiness.net/pho-bo-nam-cha" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                                   <li>
                                                      <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('12');"><i class="fa fa-retweet"></i></button>
                                                   </li>
                                                   <li>
                                                      <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('12');"><i class="fa fa-heart"></i></button>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="tab-links col-lg-12">
                              </div>
                           </div>
                        </div>
                        <script type="text/javascript"><!--
                           //Fix the product layout responsiveness
                           $(document).ready(function() {
                           	//we only want this code to execute one time even if the are several showintabs mods in the pages
                           	if (typeof showtabFLAG == 'undefined'){
                           		//Set flag
                           		showtabFLAG = true;

                           		//Columns number
                           		colsTab = $('#column-right, #column-left').length;

                           		//default values for carousel
                           		xsItems = 1;
                           		smItems = 2;
                           		mdItems = 3;
                           		lgItems = 4;

                           		//Check columns conditions
                           		if (colsTab == 2) {
                           			smItems = 1;
                           		 	mdItems = 2;
                           			lgItems = 2;
                           			$('#content .product-layout-tab').attr('class', 'product-layout-tab product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
                           			$('#content .product-layout-tab:nth-child(2n+2)').after('<div class="clearfix visible-md visible-sm"></div>');
                           		} else if (colsTab == 1) {
                           			mdItems = 2;
                           			lgItems = 3;
                           			$('#content .product-layout-tab').attr('class', 'product-layout-tab product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
                           			$('#content .product-layout-tab:nth-child(3n+3)').after('<div class="clearfix visible-lg"></div>');
                           		}else{
                           			$('#content .product-layout-tab:nth-child(4n+4)').after('<div class="clearfix"></div>');
                           		}
                           	}
                           });
                           //-->
                        </script>
                        <script type="text/javascript"><!--
                           $(document).ready(function() {
                           	$('.owl-car507b14').owlCarousel({
                           		baseClass:'owl-car-tab',
                           		itemsCustom: [ [0, xsItems], [768, smItems], [992, mdItems], [1170, lgItems]],
                           		pagination: false,
                           		navigation: true,
                           		slideSpeed: 500,
                           		scrollPerPage:false,
                           		navigationText:["<i class='fa fa-long-arrow-left'></i>","<i class='fa fa-long-arrow-right'></i>"]
                           	});
                           });
                           //-->
                        </script>
                     </div>
                     <div id="banner_page_0" class="banner_page">
                        <ul>
                           <li class="item b-stripe oll">
                              <a href="/pho-nuoc/bun-bo-hue-to-lon">
                              <img src="Content/img/qc-01-580x240.jpg" alt="quảng cáo 1" class="img-responsive">
                              </a>
                              <!-- <div class="name_banner"><a href="/pho-nuoc/bun-bo-hue-to-lon">quảng cáo 1</a></div>-->
                           </li>
                           <li class="item b-stripe event">
                              <a href="/pho-kho/pho-bo-ca-loc">
                              <img src="Content/img/qc-02-580x240.jpg" alt="quảng cáo 2" class="img-responsive">
                              </a>
                              <!-- <div class="name_banner"><a href="/pho-kho/pho-bo-ca-loc">quảng cáo 2</a></div>-->
                           </li>
                        </ul>
                        <div class="clear"></div>
                     </div>
                     <div class="h2-arviel-title">
                        <h3>Mới nhất</h3>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                           <div class="row category_product">
                              <div class="col-sm-12 col-md-12 col-lg-12">
                                 <div class="t-all-product-info">
                                    <div class="p-sign">Mới</div>
                                    <div class="t-product-img">
                                       <a href="http://19671.chilibusiness.net/pho-kho-hen">
                                       <img src="Content/img/pho_20-350x350.jpg" alt="Phở khô hến" title="Phở khô hến" class="img-responsive">
                                       </a>
                                    </div>
                                    <div class="tab-p-info">
                                       <a href="http://19671.chilibusiness.net/pho-kho-hen">Phở khô hến</a>
                                       <div class="price_product">
                                          <span class="price-new">65.000 VNĐ</span>
                                       </div>
                                       <div class="star">
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                       </div>
                                       <div class="al-btns">
                                          <button type="button" onclick="cart.add('20');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                          <ul class="add-to-links">
                                             <li><a href="http://19671.chilibusiness.net/pho-kho-hen" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                             <li>
                                                <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('20');"><i class="fa fa-retweet"></i></button>
                                             </li>
                                             <li>
                                                <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('20');"><i class="fa fa-heart"></i></button>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                           <div class="row category_product">
                              <div class="col-sm-12 col-md-12 col-lg-12">
                                 <div class="t-all-product-info">
                                    <div class="p-sign">Mới</div>
                                    <div class="t-product-img">
                                       <a href="http://19671.chilibusiness.net/pho-bo-ca-loc">
                                       <img src="Content/img/pho_1-350x350.jpg" alt="Phở khô Gia Lai" title="Phở khô Gia Lai" class="img-responsive">
                                       </a>
                                    </div>
                                    <div class="tab-p-info">
                                       <a href="http://19671.chilibusiness.net/pho-bo-ca-loc">Phở khô Gia Lai</a>
                                       <div class="price_product">
                                          <span class="price-new">22.000 VNĐ</span>
                                          <div class="clear"></div>
                                          <span class="price-old">48.000 VNĐ</span>
                                       </div>
                                       <div class="star">
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                       </div>
                                       <div class="al-btns">
                                          <button type="button" onclick="cart.add('1');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                          <ul class="add-to-links">
                                             <li><a href="http://19671.chilibusiness.net/pho-bo-ca-loc" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                             <li>
                                                <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('1');"><i class="fa fa-retweet"></i></button>
                                             </li>
                                             <li>
                                                <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('1');"><i class="fa fa-heart"></i></button>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                           <div class="row category_product">
                              <div class="col-sm-12 col-md-12 col-lg-12">
                                 <div class="t-all-product-info">
                                    <div class="p-sign">Mới</div>
                                    <div class="t-product-img">
                                       <a href="http://19671.chilibusiness.net/pho-kho-binh-dinh">
                                       <img src="Content/img/pho_19-350x350.jpg" alt="Phở Khô Bình Định" title="Phở Khô Bình Định" class="img-responsive">
                                       </a>
                                    </div>
                                    <div class="tab-p-info">
                                       <a href="http://19671.chilibusiness.net/pho-kho-binh-dinh">Phở Khô Bình Định</a>
                                       <div class="price_product">
                                          <span class="price-new">49.000 VNĐ</span>
                                       </div>
                                       <div class="star">
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                       </div>
                                       <div class="al-btns">
                                          <button type="button" onclick="cart.add('19');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                          <ul class="add-to-links">
                                             <li><a href="http://19671.chilibusiness.net/pho-kho-binh-dinh" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                             <li>
                                                <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('19');"><i class="fa fa-retweet"></i></button>
                                             </li>
                                             <li>
                                                <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('19');"><i class="fa fa-heart"></i></button>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                           <div class="row category_product">
                              <div class="col-sm-12 col-md-12 col-lg-12">
                                 <div class="t-all-product-info">
                                    <div class="p-sign">Mới</div>
                                    <div class="t-product-img">
                                       <a href="http://19671.chilibusiness.net/pho-ga">
                                       <img src="Content/img/pho_17-350x350.jpg" alt="Phở gà" title="Phở gà" class="img-responsive">
                                       </a>
                                    </div>
                                    <div class="tab-p-info">
                                       <a href="http://19671.chilibusiness.net/pho-ga">Phở gà</a>
                                       <div class="price_product">
                                          <span class="price-new">20.000 VNĐ</span>
                                          <div class="clear"></div>
                                          <span class="price-old">22.000 VNĐ</span>
                                       </div>
                                       <div class="star">
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                       </div>
                                       <div class="al-btns">
                                          <button type="button" onclick="cart.add('17');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                          <ul class="add-to-links">
                                             <li><a href="http://19671.chilibusiness.net/pho-ga" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                             <li>
                                                <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('17');"><i class="fa fa-retweet"></i></button>
                                             </li>
                                             <li>
                                                <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('17');"><i class="fa fa-heart"></i></button>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                           <div class="row category_product">
                              <div class="col-sm-12 col-md-12 col-lg-12">
                                 <div class="t-all-product-info">
                                    <div class="p-sign">Mới</div>
                                    <div class="t-product-img">
                                       <a href="http://19671.chilibusiness.net/pho-bo-tai-quang-ninh">
                                       <img src="Content/img/pho_13-350x350.jpg" alt="Phở bò tái Quảng Ninh" title="Phở bò tái Quảng Ninh" class="img-responsive">
                                       </a>
                                    </div>
                                    <div class="tab-p-info">
                                       <a href="http://19671.chilibusiness.net/pho-bo-tai-quang-ninh">Phở bò tái Quảng Ninh</a>
                                       <div class="price_product">
                                          <span class="price-new">30.000 VNĐ</span>
                                          <div class="clear"></div>
                                          <span class="price-old">33.000 VNĐ</span>
                                       </div>
                                       <div class="star">
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                       </div>
                                       <div class="al-btns">
                                          <button type="button" onclick="cart.add('13');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                          <ul class="add-to-links">
                                             <li><a href="http://19671.chilibusiness.net/pho-bo-tai-quang-ninh" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                             <li>
                                                <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('13');"><i class="fa fa-retweet"></i></button>
                                             </li>
                                             <li>
                                                <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('13');"><i class="fa fa-heart"></i></button>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                           <div class="row category_product">
                              <div class="col-sm-12 col-md-12 col-lg-12">
                                 <div class="t-all-product-info">
                                    <div class="p-sign">Mới</div>
                                    <div class="t-product-img">
                                       <a href="http://19671.chilibusiness.net/pho-bo-tai-nha-trang">
                                       <img src="Content/img/pho_7-350x350.jpg" alt="Phở bò tái Nha Trang" title="Phở bò tái Nha Trang" class="img-responsive">
                                       </a>
                                    </div>
                                    <div class="tab-p-info">
                                       <a href="http://19671.chilibusiness.net/pho-bo-tai-nha-trang">Phở bò tái Nha Trang</a>
                                       <div class="price_product">
                                          <span class="price-new">30.000 VNĐ</span>
                                          <div class="clear"></div>
                                          <span class="price-old">52.000 VNĐ</span>
                                       </div>
                                       <div class="star">
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                       </div>
                                       <div class="al-btns">
                                          <button type="button" onclick="cart.add('7');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                          <ul class="add-to-links">
                                             <li><a href="http://19671.chilibusiness.net/pho-bo-tai-nha-trang" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                             <li>
                                                <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('7');"><i class="fa fa-retweet"></i></button>
                                             </li>
                                             <li>
                                                <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('7');"><i class="fa fa-heart"></i></button>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                           <div class="row category_product">
                              <div class="col-sm-12 col-md-12 col-lg-12">
                                 <div class="t-all-product-info">
                                    <div class="p-sign">Mới</div>
                                    <div class="t-product-img">
                                       <a href="http://19671.chilibusiness.net/pho-bo-tai">
                                       <img src="Content/img/pho_5-350x350.jpg" alt="Phở bò tái" title="Phở bò tái" class="img-responsive">
                                       </a>
                                    </div>
                                    <div class="tab-p-info">
                                       <a href="http://19671.chilibusiness.net/pho-bo-tai">Phở bò tái</a>
                                       <div class="price_product">
                                          <span class="price-new">24.000 VNĐ</span>
                                          <div class="clear"></div>
                                          <span class="price-old">25.000 VNĐ</span>
                                       </div>
                                       <div class="star">
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                       </div>
                                       <div class="al-btns">
                                          <button type="button" onclick="cart.add('5');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                          <ul class="add-to-links">
                                             <li><a href="http://19671.chilibusiness.net/pho-bo-tai" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                             <li>
                                                <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('5');"><i class="fa fa-retweet"></i></button>
                                             </li>
                                             <li>
                                                <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('5');"><i class="fa fa-heart"></i></button>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                           <div class="row category_product">
                              <div class="col-sm-12 col-md-12 col-lg-12">
                                 <div class="t-all-product-info">
                                    <div class="p-sign">Mới</div>
                                    <div class="t-product-img">
                                       <a href="http://19671.chilibusiness.net/pho-bo-sai-gon">
                                       <img src="Content/img/pho_10-350x350.jpg" alt="Phở bò Sài Gòn" title="Phở bò Sài Gòn" class="img-responsive">
                                       </a>
                                    </div>
                                    <div class="tab-p-info">
                                       <a href="http://19671.chilibusiness.net/pho-bo-sai-gon">Phở bò Sài Gòn</a>
                                       <div class="price_product">
                                          <span class="price-new">18.500 VNĐ</span>
                                       </div>
                                       <div class="star">
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                       </div>
                                       <div class="al-btns">
                                          <button type="button" onclick="cart.add('10');" class="button btn-cart"><span><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</span></button>
                                          <ul class="add-to-links">
                                             <li><a href="http://19671.chilibusiness.net/pho-bo-sai-gon" class="link-wishlist" data-toggle="tooltip" title="Xem chi tiết"><i class="fa fa-eye"></i></a></li>
                                             <li>
                                                <button class="link-wishlist" type="button" data-toggle="tooltip" title="Thêm so sánh" onclick="compare.add('10');"><i class="fa fa-retweet"></i></button>
                                             </li>
                                             <li>
                                                <button type="button" data-toggle="tooltip" title="Thêm Yêu thích" onclick="wishlist.add('10');"><i class="fa fa-heart"></i></button>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="content_bottom">
                  <div class="position-display">
                     <div id="banner_page_1" class="banner_page">
                        <ul>
                           <li class="item b-stripe oll">
                              <a href="/bun-mien-banh-canh/bun-bo-hue">
                              <img src="Content/img/fishing-4.jpg" alt="Canon" class="img-responsive">
                              </a>
                              <!-- <div class="name_banner"><a href="/bun-mien-banh-canh/bun-bo-hue">Canon</a></div>-->
                           </li>
                           <li class="item b-stripe event">
                              <a href="/bun-mien-banh-canh/bun-bo-mien-trung">
                              <img src="Content/img/fishing-3.jpg" alt="Sony" class="img-responsive">
                              </a>
                              <!-- <div class="name_banner"><a href="/bun-mien-banh-canh/bun-bo-mien-trung">Sony</a></div>-->
                           </li>
                           <li class="item b-stripe oll">
                              <a href="/cac-mon-an-khac/pho-bo-tai-quang-ninh">
                              <img src="Content/img/fishing-2.jpg" alt="Nikon" class="img-responsive">
                              </a>
                              <!-- <div class="name_banner"><a href="/cac-mon-an-khac/pho-bo-tai-quang-ninh">Nikon</a></div>-->
                           </li>
                           <li class="item b-stripe event">
                              <a href="/cac-mon-an-khac/pho-kho-hen">
                              <img src="Content/img/fishing-1.jpg" alt="Kodak" class="img-responsive">
                              </a>
                              <!-- <div class="name_banner"><a href="/cac-mon-an-khac/pho-kho-hen">Kodak</a></div>-->
                           </li>
                        </ul>
                        <div class="clear"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
<?php
include_once ("Inc/footer.php");
?>
